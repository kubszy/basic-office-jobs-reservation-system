<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EquipmentWorkplace extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'equipment_id', 'workplace_id',
  ];
}
